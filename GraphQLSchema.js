const graphql = require('graphql');
const graphqlDate = require('graphql-iso-date');
const DataBase = require("./DataBase");
var db = DataBase.db;


const {
    GraphQLDate,
    GraphQLTime,
    GraphQLDateTime
  } = graphqlDate;

const {
   GraphQLObjectType,
   GraphQLID,
   GraphQLInt,
   GraphQLFloat,
   GraphQLString,
   GraphQLBoolean,
   GraphQLList,
   GraphQLSchema
} = graphql;

const AppliancesType = new GraphQLObjectType({   
  name: 'Appliances',
  fields: {
    id: { type: GraphQLID },
    productname: { type: GraphQLString },
    productimage: { type: GraphQLString },
    productdescription: { type: GraphQLString },
    ratingvalue: { type: GraphQLFloat },
    ratingbest: { type: GraphQLFloat },
    ratingreviewcount: { type: GraphQLFloat },
    brandtype: { type: GraphQLString },
    brandname: { type: GraphQLString },
    offerscurrency: { type: GraphQLString },
    offersprice: { type: GraphQLFloat },
    offersvaliduntil: { type: GraphQLString },
    itemcondition: { type: GraphQLString },
    availability: { type: GraphQLString },
    sellername: { type: GraphQLString },
    dateofupdate: { type: GraphQLDateTime }
  }
})
 
const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    appliances: {
      type: new GraphQLList(AppliancesType),
      args: { id: { type: GraphQLID },
      dateofupdate: { type: GraphQLDateTime } },
      resolve(parentValue, args) {
        var query = 'SELECT * FROM appliances ';        
        if((args.id) || (args.dateofupdate)) {
          query = query + 'WHERE';
          if(args.id){
            query = query + ' id = ' + args.id + ' ';
          }
          if(args.dateofupdate){
            var isodatetime = new Date(args.dateofupdate).toISOString();                        
            query = query + ' dateofupdate >= \'' + isodatetime + '\' ';
          }           
        }                   
        query = query + 'ORDER BY id ASC';
        return db.manyOrNone(query)
          .then(data => {
            return data;
          })
          .catch(err => {
            console.log(' Error ');             
            return 'The error is', err;
          });
      }
    }
  }
})

module.exports = new GraphQLSchema({
   query: RootQuery
})


