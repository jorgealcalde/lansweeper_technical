const express = require('express');
const expressGraphQL = require('express-graphql');
const schema = require('./GraphQLSchema');
const app = express();
const fs = require('fs');
const DataBase = require("./DataBase");

const Folderdishwashers = 'Scraper_dishwashers/outputUrlsDATA';
const Foldersmallappliances = 'Scraper_small-appliances/outputUrlsDATA';

app.use('/graphql', expressGraphQL({
   schema,
   graphiql: true
}))

app.get('/updateBBDD/dishwashers', function(req, res){    
    try{      
      var obj = JSON.parse(fs.readFileSync(Folderdishwashers + '.json', 'utf8'));      
      DataBase.updateTableDB(obj, 'dishwashers');      
      var now = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '').replace(/:/g, '');
      fs.renameSync(Folderdishwashers + '.json', Folderdishwashers + now + '.json', function(err) {
        if ( err ) console.log('ERROR: ' + err);
      });
      res.sendStatus(200);
    }
    catch (e) {
      console.log('Error adding data to BBDD: ' + e);        
    }    
  })

app.get('/updateBBDD/smallappliances', function(req, res){
    try{
      var obj = JSON.parse(fs.readFileSync(Foldersmallappliances + '.json', 'utf8'));      
      DataBase.updateTableDB(obj, 'smallappliances');
      var now = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '').replace(/:/g, '');
      fs.renameSync(Foldersmallappliances + '.json', Foldersmallappliances + now + '.json', function(err) {
        if ( err ) console.log('ERROR: ' + err);
      });      
      res.sendStatus(200);
    }
    catch (e) {
      console.log('Error adding data to BBDD: ' + e);        
    }    
  })

  app.get('/', function(req, res){    
    res.send('Try with:</br>' +
      'I  - /updateBBDD/dishwashers --> It updates the appelances data from scrape to DB </br>' +   
      'II - /updateBBDD/smallappliances --> It updates the appelances data from scrape to DB </br>' +   
      'II - /graphql --> GraphQL Interface to check the updated data on BD');
  })  

app.listen(4000, () => {
   console.log('Listening on port 4000...')    
})

