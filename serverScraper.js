const express = require('express');
const fs      = require('fs');
const request = require('request');
const cheerio = require('cheerio');
const app     = express();

const urldishwashers = 'https://www.appliancesdelivered.ie/dishwashers'; 
const urlsmallappliances = 'https://www.appliancesdelivered.ie/search/small-appliances';
const Folderdishwashers = 'Scraper_dishwashers/';
const Foldersmallappliances = 'Scraper_small-appliances/';

var currentWEB = '';
var nextURL = '';
var scrapeWorking = false;
var id = 0;
var pageNumber = 1;
var currentPageNumber = 1;
var jsonarr = [];

function scrapePage(req, res){ 
  var now = new Date().toISOString();   
  return new Promise(function(resolve, reject){
    request(nextURL, function(error, response, html){
    
      if(!error){
        var $ = cheerio.load(html);      
        var urlGQL;
        var next = '';

        console.log('Starting scrape @ page: ' + pageNumber);

        $('div[class="product-image col-xs-4 col-sm-4"]').filter(function(){        
          var data = $(this);
          var itemURL = data.find('a').attr('href');
          id++;
          var jsonItem = { id : "", MainPage : "", URLMAinPage : "", URL : "", Date : "", Data : ""};
          jsonItem.id = id;
          jsonItem.MainPage = pageNumber;
          jsonItem.URLMAinPage = nextURL;
          jsonItem.URL = itemURL;                    
          jsonItem.Date = now;  
          jsonarr.push(jsonItem);
        })

        $('p[class="col-md-10 result-list-pagination"]').filter(function(){        
          var data = $(this).find('a');
          var length = data.length;
          var next = '';
          for (var i=0; i<length; i++){
            var nodeText = data.slice(i).text();            
            if (nodeText.startsWith('next')){              
              next = data.slice(i).attr('href');
              pageNumber++;
            }
          }                    
          console.log('-----------> Next: ' + next);
          nextURL = next;                  
        })
      }
      else{
        console.log('URL ERROR');
        nextURL = '';
      }

      if (pageNumber > currentPageNumber){       
        currentPageNumber++;
       console.log(' Moving to Next Web.........');
       resolve(nextURL);
      }
      else{ 
        resolve(''); 
        console.log(' Ending............');      
      }          
    })  

  })
}

function scrapeItem(req, res,nextURL){    
  return new Promise(function(resolve, reject){
    request(nextURL, function(error, response, html){
    
      if(!error){
        var $ = cheerio.load(html);
        var itemData = '';      

        $('script[type="application/ld+json"]').filter(function(){        
          var data = $(this);       
          itemData = data.html().trim();          
        })
        resolve(itemData);
      }               
    })
  })
}


function procesingScrapeItems(req, res){  
  scrapeItem(req, res, jsonarr[id].URL).then(function(result){    
    if(result){
      try{        
        result = result.replace(/(\r\n\t|\n|\r\t)/gm,"");
        //console.log(result);
        jsonarr[id].Data = JSON.parse(result);
      } catch (e) {
        console.log('Error adding data: ' + e);        
      }
    } 

    console.log("Item: " + id + ' done:' + jsonarr[id].URL);   
    id++;    
    if (id < jsonarr.length){
      procesingScrapeItems(req, res);
    }
    else{
      fs.writeFileSync(currentWEB + 'outputUrlsDATA.json', JSON.stringify(jsonarr, null, 4), function (err) {
        if (err) throw err;     
       });     
       scrapeWorking = false;       
       console.log(' File '+currentWEB+'outputUrlsDATA.json saved');
       console.log(' This is the END....');
    }    
  },
  function(err){
      console.log('ScrapeItemsERROR: ' + err);
  })
}


function procesingScrapePages(req, res){
  scrapePage(req, res).then(function(result) {
    if (result){                       
      procesingScrapePages(req, res);
    }
    else{
      fs.writeFileSync(currentWEB+'outputUrls.json', JSON.stringify(jsonarr, null, 4), function (err) {
        if (err) throw err;      
      });
       
       console.log("Finished: " + jsonarr.length + ' items');      
       id = 0;
       procesingScrapeItems(req, res);       

       console.log(' File '+currentWEB+'outputUrls.json saved');
       res.send('Check out '+currentWEB+'outputUrls.json!')  
       console.log("Finished ALL pages, starting with");      
    }    
  },
  function(err){
      console.log('scrapePageERROR: ' + err);
  })
}


var dishwashersWorking = false;
var smallappliancesWorking = false;

app.get('/scrape/dishwashers', function(req, res){
  if (!scrapeWorking){
    scrapeWorking = true;
    nextURL = urldishwashers;
    currentWEB = Folderdishwashers;
    jsonarr.length = 0;
    id = 0;
    pageNumber = 1;
    currentPageNumber = 1;  
    console.log("scraping: " + nextURL);  
    procesingScrapePages(req, res);   
  }
  else{res.sendStatus(200);}
})

app.get('/scrape/smallappliances', function(req, res){
  if(!scrapeWorking){
    scrapeWorking = true;
    nextURL = urlsmallappliances;
    currentWEB = Foldersmallappliances;
    jsonarr.length = 0;
    id = 0;
    pageNumber = 1;
    currentPageNumber = 1;    
    console.log("scraping: " + nextURL);  
    procesingScrapePages(req, res);   
  }
  else{res.sendStatus(200);}
})

app.get('/scrape', function(req, res){
  //res.sendStatus(200);
  res.send('Try with:</br>' +
    'I - /scrape/dishwashers --> ' + urldishwashers + '</br>'+   
    'II - /scrape/smallappliances --> ' + urlsmallappliances);
})

app.listen('8081')
console.log('Magic happens on port 8081 /scrape');
exports = module.exports = app;
