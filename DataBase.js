const connectionStrings = 'postgres://postgres:Lutien12@localhost:5432/todo';
const pgp = require('pg-promise')();
var db = pgp(connectionStrings);


function showTableDB() {  
  const query = ('SELECT * FROM appliances ORDER BY id ASC');   
  db.manyOrNone(query)
    .then(data => {      
      return data;           
    })
    .catch(err => {      
      return 'The error is', err;            
    }); 
}

function removeTableDB() {        
  const query = ('DROP TABLE appliances');
  db.one(query)
    .then(data => {
      return data;
    })
    .catch(err => {
      return 'The error is', err;
    }); 
}
 
function createTableDB() {       
  const query = ( 
   'CREATE TABLE appliances(id SERIAL PRIMARY KEY, ' + 
   'productName text, ' + 
   'productImage text, ' + 
   'productDescription text, ' +     
   'ratingValue real, ' + 
   'ratingBest real, ' + 
   'ratingReviewCount real, ' +    
   'brandType text, ' + 
   'brandName text, ' + 
   'offersCurrency VARCHAR(10), ' + 
   'offersPrice real, ' + 
   'offersValidUntil text, ' + 
   'itemCondition text, ' + 
   'availability text, ' + 
   'sellerName text, ' + 
   'dateofupdate timestamp )');  
  db.one(query)
    .then(data => {
      return data;
     })
     .catch(err => {
      return 'The error is', err;
     }); 
}

function InsertInTableDB(productName, productImage, productDescription, ratingValue, ratingBest, ratingReviewCount, brandType,
  brandName, offersCurrency, offersPrice, offersValidUntil, itemCondition, availability, sellerName, dateofupdate) {   
  const query = db.query('INSERT INTO appliances' +
   '(productName, productImage, productDescription, ratingValue, ratingBest, ratingReviewCount, brandType, ' +
   'brandName, offersCurrency, offersPrice, offersValidUntil, itemCondition, availability, sellerName, dateofupdate) ' +
   'values($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15)',
   [productName, productImage, productDescription, ratingValue, ratingBest, ratingReviewCount, brandType,
   brandName, offersCurrency, offersPrice, offersValidUntil, itemCondition, availability, sellerName, dateofupdate]);
  db.one(query)
    .then(data => {       
      return ('INSERT OK');
    })
    .catch(err => {      
      return 'INSERT ERROR: ', err;
    });
}  
   
function updateTableDB(obj, type) {
  try{       
    for(var i=0; i < obj.length; i++) {
      if(obj[i].Data) {              
         InsertInTableDB(               
         obj[i].Data.productName,
         obj[i].Data.image,
         obj[i].Data.description,
         obj[i].Data.aggregateRating.ratingValue,
         obj[i].Data.aggregateRating.bestRating,
         obj[i].Data.aggregateRating.reviewCount,
         type,
         obj[i].Data.offers.brandName,
         obj[i].Data.offers.priceCurrency,
         obj[i].Data.offers.price,
         obj[i].Data.offers.priceValidUntil,
         obj[i].Data.offers.itemCondition,
         obj[i].Data.offers.availability,
         obj[i].Data.offers.seller.name,
         obj[i].Date);
      }  
    }
  }
  catch (e) {
    throw(' Loop('+i+'): ' + e)        
  }    
}  

module.exports.db = db;
module.exports.removeTableDB = removeTableDB;
module.exports.createTableDB = createTableDB;
module.exports.updateTableDB = updateTableDB;
module.exports.showTableDB = showTableDB;
module.exports.InsertInTableDB = InsertInTableDB