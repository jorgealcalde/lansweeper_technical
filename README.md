# Lansweeper_Technical

Jorge Alcalde 
j.alcalde.a@gmail.com
tlf: +34 686159926

-	Report        - 

In my point of view, as a first approach of the requirements we can divide the task in three parts or microservices. 
 
I: A first microservice to scrape the AppliancesDeliverd.ie dishwashers and small-appliances, and to save the data in a proper format.

II: A second microservice that manages the DB resources, recovers the scraping data and saves it in the DB in an appropriate way.

III: A third microservice that holds the web interface and consumes the resources of these microservices.


I - Scraping (ServerScraper.js): As a first approach, I developed a microservice that scrapes all items of the URLs provided and their next ones. It surfs through all next-WEBs links until the end of the advertised items. It recovers the needed metadata and it saves it in some files, in JSON format. 

This first approach accomplishes with the separation between the web side resources and the update of Data, because the WEB will get the data from the DB, and this one, just will be updated once we get a proper metadata from the scraping.
As starting I have added to the repository, the JSON files of the last scraping I made:

Scraper_dishwashers/outputUrlsDATA.json
Scraper_small-appliances/outputUrlsDATA.json

Packages: express, fs, request & cheerio.


II -  DB services (DataBase.js): A resource that manages, creates, removes, inserts and reads a DB table. And also updates the data from the scraping resource, and as we saw in the previous definition, it is safe from the scraping. 

Packages: postgres - pg-promise.


III - (GraphQLSchema.js): It reads the data from the DB and shows it as GraphQL.

Packages: graphql &postgres - pg-promise.



-As I only had the weekend to work on it, it is not as polished and operative as I really desired, the first point it is 100% operative (always can be improved. The second point it is not as operative as I desired, it has things to be polished. The third point doesn’t have a pretty interface build in React, I couldn’t get to it, I make use of the Graphql package interface to surf the appliances.

1 - For Scraping, just launch ServerScraper.js and follow the console indications.

   I  - http://localhost:8081/scrape/dishwashers
   II - http://localhost:8081/scrape/smallappliances

2 - For saving the data to the DB, launch server.js

      I - http://localhost:4000/updateBBDD/dishwashers
 
      II - http://localhost:4000/updateBBDD/smallappliances 

3 – Launch server.js and follow the console indications.

      I - http://localhost:4000/graphql


•	For any questions or enquiries, please do not doubt to contact me.


*************************************************************************************
     Update 09/12/2018 
*************************************************************************************

-The second point (DB management) has been polished and improved (100% operative).
Added the dateofupdate field in order to filter new and old items.
Once the data from the scraping has been saved in the DB, the names of the files are renamed.

-The third point (GraphQL) can filter data by ID and dateofupdate.
-Update of appliances JSON files (09/12/2018 22:00:00).
Scraper_dishwashers/outputUrlsDATA.json
Scraper_small-appliances/outputUrlsDATA.json

 
 
  


